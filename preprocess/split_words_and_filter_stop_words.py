# encoding=utf-8
import csv
import jieba
import jieba.analyse
import re
import os
import timeit
import sys
reload(sys)
sys.path.append('..')
import utilities.utilities as utilities
sys.setdefaultencoding('utf-8')


wordsDict = {}
path = '../data/raw_data/'
if __name__ == '__main__':
    utilities.set_field_size_limit()
    stop_words = utilities.read_stopwords('../data/aux/stop_words')
    content = []

    i = 0
    with open(os.path.join(path, 'item_raw.csv')) as csvFile:
        reader = csv.DictReader(csvFile)
        for row in reader:

            if i % 1000 == 0:
                print i
            i += 1

            seg_content = jieba.cut(row['content'], cut_all = False)
            seq_title = jieba.cut(row['title'], cut_all = False)

            for word in seq_title:
                if(stop_words.has_key(word) == False):
                    content.append(word)
                    content.append(' ')
            content.append('\n')

            for word in seg_content:
                if(stop_words.has_key(word) == False):
                    content.append(word)
                    content.append(' ')
            content.append('\n')

    with open(os.path.join(path+'item_title_content_processed.txt'),'w') as f:
        f.write(''.join(content))
    print('finished !')
