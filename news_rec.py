# encoding=utf-8
import os
import csv
import re
import time
import jieba
import gensim
import copy
import User_Info
import News_Info
import utilities.utilities as utilities
from copy import deepcopy
import numpy as np
from Queue import PriorityQueue as PQueue
from gensim import utils, matutils
from gensim.models import word2vec
DATABASE_PATH = 'database'
USER_PROCESSED_FILE = 'user-small.csv'
LDA_MODEL_FILE = os.path.join('model/LDA', '1501566850', 'checkpoints','LDA.model')
WOED2VEC_MODEL_FILE = os.path.join('model/Word2Vec', '1500975010', 'checkpoints','UScities.model.bin')
CORPUS_DICT_FILE = 'data/aux/word_cnt_dict.p'
STOP_WORDS_FILE = 'data/aux/stop_words'
NEWS_FILE = 'data/uniq_sogou_raw_corpus.txt'

#LDA topic numbers
TOPIC_NUM = 10


LDA_model = gensim.models.LdaModel.load(LDA_MODEL_FILE)
corpus_dictionary = gensim.corpora.Dictionary.load(CORPUS_DICT_FILE)
word2vec_model = word2vec.KeyedVectors.load_word2vec_format(WOED2VEC_MODEL_FILE, binary=True)

if __name__ == '__main__':
    user_info_list, user_id_dict = utilities.load_user_list(os.path.join(os.getcwd(),DATABASE_PATH, USER_PROCESSED_FILE))
    cur_user_id = '5218791'
    cur_user = user_info_list[user_id_dict[cur_user_id]]

    stop_words = utilities.read_stopwords(os.path.join(os.getcwd(),STOP_WORDS_FILE))

    today_data = time.strptime('2012-06-01','%Y-%m-%d')
    timestamp = time.mktime(today_data)
    print timestamp

    print('begin to load news...')
    news_info_list = utilities.load_news(os.path.join(os.getcwd(), NEWS_FILE))
    print('loading news finished!')

    #sort news by pubtime
    news_info_list.sort(lambda item1, item2 : cmp(time.mktime(item1.pub_time), time.mktime(item2.pub_time)))

    news_index = 0
    day_cnt = 1
    while True:

        today_news_list = []
        while(True):
            if(news_index >= len(news_info_list)):
                break
            cur_news_item = news_info_list[news_index]
            if(time.mktime(cur_news_item.pub_time) > timestamp):
                break

            today_news_list.append(cur_news_item)
            news_index += 1

        print '今天是：～～～～～～～～～～～～～'
        print today_data
        print('今天共有 '+str(len(today_news_list))+'条新闻')
        utilities.calc_topic_vec(today_news_list, LDA_model,corpus_dictionary, word2vec_model, stop_words)
        user_top_3 = utilities.find_user_topic_top_3(cur_user.deep_copy())

        num = [4, 3, 2]
        print('===================================')
        print('recommendate from LDA model')
        print('===================================')
        for i in range(3):
            top_news = utilities.find_topic_top_news(today_news_list, user_top_3[i], num[i], TOPIC_NUM)
            for top_news_index in top_news:
                print(str(top_news_index) + ': ' + today_news_list[top_news_index].title)
        print('==================================')


        #word2vec
        nearest_list = utilities.find_nearest_news(cur_user.deep_copy(), today_news_list, 6)
        print('recommendate from nearest vector')
        print('===================================')
        for nearest_news_index in nearest_list:
            print(str(nearest_news_index) +': ' + today_news_list[nearest_news_index].title)
        print('==================================')

        print('输入你感兴趣的编号，以回车结束（输入多条用空格分开，可以不输入）')
        user_action = raw_input()
        user_action_list = user_action.split(' ')
        #print user_action
        day_cnt += 1
        today_data = time.strptime('2012-06-'+str(day_cnt),'%Y-%m-%d')


        timestamp = time.mktime(today_data)

        for user_action in user_action_list:
            if user_action.isdigit():
                cur_user = utilities.update_user_preference(cur_user.deep_copy(), today_news_list[int(user_action)])

        #print cur_user.topic_vec




    # while(True):
