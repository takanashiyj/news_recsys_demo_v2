# encoding=utf-8
import csv
import re
import gensim
import jieba
import time
import os
import sys
import pandas as pd
import numpy as np
from gensim.models import word2vec
reload(sys)
sys.setdefaultencoding('utf-8')

LDA_MODEL_FILE = os.path.join('./runs/LDA', '1501566850', 'checkpoints','LDA.model')
WOED2VEC_MODEL_FILE = os.path.join('./runs', '1500975010', 'checkpoints','UScities.model.bin')
CORPUS_DICT_FILE = './data/word_cnt_dict.p'
STOP_WORDS_FILE = './data/stopWords'
CSV_SAVE_PATH = './news_recsys_demo/database'
def csv_memory_init():
    """Set csv memory
        Default value for csv memory is too small for long content news
    """
    maxInt = sys.maxsize
    decrement = True
    while decrement:
        # decrease the maxInt value by factor 10
        # as long as the OverflowError occurs.
        decrement = False
        try:
            csv.field_size_limit(maxInt)
        except OverflowError:
            maxInt = int(maxInt/10)
            decrement = True
stop_words = {}
def read_stopwords():
    with open(STOP_WORDS_FILE,'r') as f:
        for line in f.readlines():
            stop_words[line.split('\n')[0]]=1

def process_title(title):
    print title
    title_cut = jieba.cut(title, cut_all=False)
    title_filter = list(filter(lambda x: not stop_words.has_key(x.encode('utf-8')), title_cut))
    return title_filter

"""
ret: timestamp(float)
     -1 if time is NULL
"""
def process_time(pubtime):
    if not('NULL' in pubtime):
        date = re.split('年|月|日|\r|\n', pubtime)[:3]
        date_formulate = time.strptime(date[0]+'-'+date[1]+'-'+date[2],'%Y-%m-%d')
        timestamp = time.mktime(date_formulate)
        return timestamp
    else:
        return -1

item_record_list = []
index_list = []
if __name__ == '__main__':
    csv_memory_init()
    read_stopwords()
    #load model
    LDA_model = gensim.models.LdaModel.load(LDA_MODEL_FILE)
    corpus_dictionary = gensim.corpora.Dictionary.load(CORPUS_DICT_FILE)
    word2vec_model = word2vec.KeyedVectors.load_word2vec_format(WOED2VEC_MODEL_FILE, binary=True)
    with open('./data/item.csv') as csvFile:
        rows = csv.DictReader(csvFile)
        for row in rows:
            id = str(row['itemId'])
            title = row['title']
            pubtime = process_time(row['pubtime'])
            title_filter = process_title(title)

            # calc topic vec
            title_processed = corpus_dictionary.doc2bow(title_filter)
            topic_vec = [item[1] for item in LDA_model[title_processed]]

            # calc word2vec
            word_vec = []
            for word in title_filter:
                if word in word2vec_model.vocab:
                    word_vec.append(word2vec_model.wv[word])
            #word_vec =[word2vec_model.wv[word] for word in title_filter]
            title_vec = np.mean(word_vec, axis = 0).tolist()

            print(title_vec)
            item_record = [id, pubtime, topic_vec, title_vec]
            item_record_list.append(item_record)
            index_list.append(id)

    df = pd.DataFrame(item_record_list, columns = ['itemid', 'pubtime', 'topic_vec', 'title_vec'])

    df.to_csv(os.path.join(CSV_SAVE_PATH,'item_rename.csv'))
