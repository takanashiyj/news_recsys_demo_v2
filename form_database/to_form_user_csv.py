# encoding=utf-8
import pandas as pd
import numpy as np
import os
import csv
import sys
import math
from Queue import PriorityQueue as PQueue
reload(sys)

sys.setdefaultencoding('utf-8')
CSV_SAVE_PATH = './news_recsys_demo/database'
USER_RAW_FILE = './data/user.csv'
USER_PROCESSED_FILE = 'user.csv'
RECENT_READ_NUM = 10

user_info_list = []
user_id_dict = {}
class UserInfo:
    def __init__(self, userid, read_sum = 0, topic_vec = np.array([]), recent_read_queue = PQueue()):
        self.userid = userid
        self.read_sum = read_sum
        self.topic_vec = topic_vec
        self.recent_read_queue = recent_read_queue

def read_vec(item, column):
    # print 'read_vec...'
    # print 'column: '+column
    # print item[column]
    item_topic_vec = item[column].values[0].split('[')[1].split(']')[0].split(',')
    item_topic_vec_float = [float(topic) for topic in item_topic_vec]
    return item_topic_vec_float
def csv_memory_init():
    """Set csv memory
        Default value for csv memory is too small for long content news
    """
    maxInt = sys.maxsize
    decrement = True
    while decrement:
        # decrease the maxInt value by factor 10
        # as long as the OverflowError occurs.
        decrement = False
        try:
            csv.field_size_limit(maxInt)
        except OverflowError:
            maxInt = int(maxInt/10)
            decrement = True

cnt = 0
if __name__ == '__main__':
    csv_memory_init()
    item_df = pd.read_csv(os.path.join(CSV_SAVE_PATH, 'item_rename.csv'))
    user_df = pd.DataFrame(columns = [ u'topic_vec', u'recent_read_vec', u'sum'])
    with open(USER_RAW_FILE, 'r')as csvFile:
        rows = csv.DictReader(csvFile)
        for row in rows:
            if cnt % 1000 == 0:
                print cnt
            cnt += 1
            user_id = str(row['userId'])
            item_id = row['itemId']
            timestamp = row['timestamp']

            index = -1

            item = item_df.loc[item_df['itemid'] == int(item_id)]
            # item_topic_vec = item['topic_vec'].values[0].split('[')[1].split(']')[0].split(',')
            # item_title_vec = item['title_vec'].values[0].split('[')[1].split(']')[0].split(',')
            # item_topic_vec_float = [float(topic) for topic in item_topic_vec]
            # item_topic_vec_array = np.array(item_topic_vec_float)
            # print 'item_id: '+str(item_id)
            # print 'title_vex'
            # print item['title_vec'].values[0]
            if('[' not in str(item['title_vec'].values[0])):
                continue
            item_topic_vec_array = np.array(read_vec(item, 'topic_vec'))
            item_title_vec = read_vec(item, 'title_vec')

            #print item_id
            #print item_topic_vec_array.shape[0]
            # bug 我也不知道为什么啊...topic vec有！=10的...
            if(item_topic_vec_array.shape[0] != 10):
                continue
            if(user_id_dict.has_key(user_id)):
                index = user_id_dict[user_id]

                user_topic_vec = user_info_list[index].topic_vec
                read_sum = user_info_list[index].read_sum
                #print user_topic_vec
                #print item_topic_vec_array
                user_topic_vec = user_topic_vec * read_sum / (read_sum + 1) + item_topic_vec_array * 1 / (read_sum + 1)
                recent_read_queue = user_info_list[index].recent_read_queue
                if(recent_read_queue.qsize() >= RECENT_READ_NUM):
                    recent_read_queue.get()
                recent_read_queue.put((float(timestamp), item_title_vec))

                user_info_list[index].topic_vec = user_topic_vec
                user_info_list[index].read_sum = read_sum + 1
                user_info_list[index].recent_read_queue = recent_read_queue
            else:

                index = len(user_id_dict)
                user_id_dict[user_id] = index
                recent_read_queue = PQueue()
                recent_read_queue.put((float(timestamp), item_title_vec))
                user_info = UserInfo(user_id, 1, item_topic_vec_array, recent_read_queue)
                user_info_list.append(user_info)

    user_cnt = 0
    userFileHeader = ['userId', 'read_sum',  'topic_vec', 'recent_read_queue']
    with open(os.path.join(CSV_SAVE_PATH, USER_PROCESSED_FILE), 'w') as userCSVFile:
        userWriter = csv.writer(userCSVFile)
        userWriter.writerow(userFileHeader)

        for user in user_info_list:
            if user_cnt % 1000 == 0:
                print user_cnt
            user_cnt+=1
            user_info = []
            queue_list = []
            
            while not user.recent_read_queue.empty():
                queue_list.append(user.recent_read_queue.get())
            user_info.append(user.userid)
            user_info.append(user.read_sum)
            user_info.append(user.topic_vec.tolist())
            user_info.append(queue_list)
            userWriter.writerow(user_info)





    #         sum = 1
    #         if not user_id in user_df.index:
    #             user_df.set_value(user_id,u'sum', sum)
    #         else:
    #             sum = user_df.loc[user_id][u'sum'] + 1
    #             user_df.set_value(user_id,u'sum', sum)
    #
    #         if(item_id != '-1'):
    #             print(item_id)
    #             item = item_df.loc[item_df['itemid'] == int(item_id)]
    #             item = item[u'topic_vec'][0].split('[')[1].split(']')[0].split(',')
    #             item_topic_vec = [float(x) for x in item]
    #             print item_topic_vec
    #             if(sum == 1):
    #                 user_df.set_value(user_id,u'topic_vec', item_topic_vec)
    #             else:
    #                 topic_vec_odd = user_df[user_id][u'topic_vec']
    #
    #                 print topic_vec_odd
    #             raw_input()
    #
    # print user_df
            # if(user_df.loc('11') == -1):
            #     user_df = pd.DataFrame(index = [user_id],column = {u'sum': [1]})
            #     user_df.append(user_df)
            # else:
            #     user_df[user_id][u'sum'] = 2






    #         user_list.append([user_id, item_id, timestamp])
    #
    # user_df_raw = pd.DataFrame(user_list, columns = [u'user_id', u'item_id', u'timestamp'])
    # print user_df_raw
    #


    # print data
    # row = data.loc[data['itemid'] == 100648598]
    # print row['itemid']
