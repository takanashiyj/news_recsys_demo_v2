class NewsInfo:
    """
    Attributes:
        title: news title (raw data)
        content: news title (raw data)
        title_vec: title vector using word2vec
        topic vec: a array of LDA probility distribution of news topic
    """
    def __init__(self, pub_time, title, content, title_vec = [], topic_vec = []):
        self.pub_time = pub_time
        self.title = title
        self.content = content
        self.title_vec = title_vec
        self.topic_vec = topic_vec
