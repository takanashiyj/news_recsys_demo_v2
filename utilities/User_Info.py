from copy import deepcopy
import numpy as np
from Queue import PriorityQueue as PQueue

class UserInfo:
    """

    Attributes:
        userid:
        read_sum: total news number the user read
        topic_vec: a array of LDA probility distribution of the user has read in each topic
        recent_read_queue: a series of tuple items saved in priority queue stands for the user
            recent read 10 pieces of news
            each tuple is like this: (timestamp, [, , ... , ]) means timestamp and title vector
            from word2vec of the recent read news
    """
    def __init__(self, userid, read_sum = 0, topic_vec = np.array([]), recent_read_queue = PQueue()):
        self.userid = userid
        self.read_sum = read_sum
        self.topic_vec = topic_vec
        self.recent_read_queue = recent_read_queue

    def deep_copy(self):
        userid = deepcopy(self.userid)
        read_sum = deepcopy(self.read_sum)
        topic_vec = deepcopy(self.topic_vec)
        queue_list = []
        queue = PQueue()
        while not self.recent_read_queue.empty():
            element = self.recent_read_queue.get()
            queue_list.append(element)
        for element in queue_list:
            self.recent_read_queue.put(element)
            queue.put(element)
        return UserInfo(userid, read_sum, topic_vec, queue)
