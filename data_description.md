# 1. pkbigdata数据集
## 1.1raw_data

### 1.1.1 TrainData.txt 

 pkbigdata的原始数据[http://www.pkbigdata.com/static_page/m/cmptIndex.html?cmptId=125]

* 数据集描述

在本次竞赛中，我们从国内某著名财经新闻网站—财新网随机选取了10000名用户，并抽取了这10000名用户在2014年3月的所有新闻浏览记录，每条记录包括用户编号、新闻编号、浏览时间（精确到秒）以及新闻文本内容，其中用户编号已做匿名化处理，防止暴露用户隐私。

训练集数据每一行为一个浏览记录，该行浏览记录包含6个字段，分别记录一下信息：用户编号 新闻编号 浏览时间 新闻标题 新闻详细内容 新闻发表时间，字段之间用table符即”\t”隔开，文本编码为utf8编码格式，如下为截取训练集中的一行：

![img1](img1.png)

## 1.2 corpus

### 1.2.1 pkbigdata_title_content_corpus.txt

在item_raw.csv的基础上，提取新闻的标题和内容后进行分词，同时过滤stop words。共有6183条新闻（已去重）



## 1.3 database
### 1.3.1 item_raw.csv & user_raw.csv

从TrainData.txt拆分成item和user两个表

item_raw.csv如下，共有6183条新闻，已去重。

| itemId    | title     | content              | pubtime          |
| --------- | --------- | -------------------- | ---------------- |
| 100648598 | 消失前的马航370 | 【财新网】...平均使用年龄达14年。■ | 2014年03月08日12:31 |

user_raw.csv如下，共有116225条记录。

| userid  | itemid    | timestamp  |
| ------- | --------- | ---------- |
| 5218791 | 100648598 | 1394463264 |



# 2. 搜狗数据集

## 2.1 raw_data

### 2.1.1 sogou_raw_data.txt

搜狗新闻的raw data[http://www.sogou.com/labs/resource/ca.php]

数据格式为

<doc>

<url>页面URL</url>

<docno>页面ID</docno>

<contenttitle>页面标题</contenttitle>

<content>页面内容</content>

</doc>

注意：content字段去除了HTML标签，保存的是新闻正文文本

共1294233条新闻。

### 2.1.2 uniq_sogou_raw_data.txt

sogou_raw_data.txt按标题去重，共795862条新闻。

## 2.2 corpus

### 2.2.1 sogou_title_content_corpus.txt

包含页面标题和内容分词和停词后的结果。标题和内容各占一行。共795862条新闻。（已去重）

# 3. corpus

## 3.1 combine_corpus.txt

将两个数据集的语料合起来，共802045条新闻语料。

# 4. aux

## 4.1 stop_words停词表



